package org.dhiren.springframework.actuator.endpoint;

import org.springframework.boot.actuate.endpoint.AbstractEndpoint;
import org.springframework.stereotype.Component;

class Information {

    private String status;
    private int id;

    public Information(String status, int id) {
        this.id = id;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

@Component
public class CustomEndpoint extends AbstractEndpoint<Information> {

    public CustomEndpoint() {
        super("status", false);
    }

    @Override
    public Information invoke() {
        return new Information("UP",200);
    }
}
