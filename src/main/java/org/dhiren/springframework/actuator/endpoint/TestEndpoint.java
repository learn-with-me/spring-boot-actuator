package org.dhiren.springframework.actuator.endpoint;

import org.springframework.boot.actuate.endpoint.AbstractEndpoint;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class TestEndpoint extends AbstractEndpoint<List<String>> {

    public TestEndpoint() {
        super("test", false);
    }

    @Override
    public List<String> invoke() {
        return Arrays.asList("Dhiren","Dhiren","Dhiren");
    }
}
