package org.dhiren.springframework.actuator.adapter;

import org.dhiren.springframework.actuator.endpoint.CustomEndpoint;
import org.springframework.boot.actuate.endpoint.mvc.EndpointMvcAdapter;
import org.springframework.stereotype.Component;

@Component
public class CustomEndpointMvcAdapter extends EndpointMvcAdapter {

    public CustomEndpointMvcAdapter(CustomEndpoint delegate) {
        super(delegate);
    }
}
