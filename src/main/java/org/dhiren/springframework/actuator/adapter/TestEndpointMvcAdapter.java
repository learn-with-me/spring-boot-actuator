package org.dhiren.springframework.actuator.adapter;

import org.dhiren.springframework.actuator.endpoint.TestEndpoint;
import org.springframework.boot.actuate.endpoint.mvc.EndpointMvcAdapter;
import org.springframework.stereotype.Component;

@Component
public class TestEndpointMvcAdapter extends EndpointMvcAdapter {
    public TestEndpointMvcAdapter(TestEndpoint delegate) {
        super(delegate);
    }
}
