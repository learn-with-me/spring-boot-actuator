package org.dhiren.springframework.actuator.indicator;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component("AppHealth")
public class MyHealthIndicator implements HealthIndicator {

    @Override
    public Health health() {
        Random random = new Random();
        if(random.nextBoolean())
            return Health.down().withDetail("ERR-001", "Ahh Ohh Service is not Available").status("500").build();
        return Health.up().build();
    }
}
