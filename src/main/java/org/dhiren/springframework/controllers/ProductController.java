package org.dhiren.springframework.controllers;

import org.dhiren.springframework.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.boot.actuate.metrics.GaugeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by jt on 1/20/16.
 */
@Controller
public class ProductController {

    private ProductService productService;
    private CounterService counterService;
    private GaugeService gaugeService;

    @Autowired
    public ProductController(ProductService productService,
                             @Qualifier("counterService") CounterService counterService, @Qualifier("gaugeService") GaugeService gaugeService) {
        this.productService = productService;
        this.counterService = counterService;
        this.gaugeService = gaugeService;
    }

    @RequestMapping("/product/{id}")
    public String getProductById(@PathVariable Integer id, Model model){

        model.addAttribute("product", productService.getProduct(id));
        counterService.increment("Total Product API Count");
        gaugeService.submit("Page Count", 1001);
        return "product";
    }
}
