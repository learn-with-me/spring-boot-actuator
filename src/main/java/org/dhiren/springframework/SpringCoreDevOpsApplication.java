package org.dhiren.springframework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;
import org.springframework.hateoas.config.EnableHypermediaSupport;

import static org.springframework.hateoas.config.EnableHypermediaSupport.HypermediaType.HAL;

@EnableHypermediaSupport(type = HAL)
@SpringBootApplication(exclude = HypermediaAutoConfiguration.class)
public class SpringCoreDevOpsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCoreDevOpsApplication.class, args);
	}
}
